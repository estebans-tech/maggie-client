import React, { Component } from 'react'
// import React from 'react'
import './index.scss'
import 'bulma'
import RecorderJS from 'recorder-js'
// import Recorder from './Components/Recorder'
import { getAudioStream, exportBuffer } from './utilities/audio';


class MaggieRecorder extends Component {
  constructor(props) {
    super(props)

    this.state = {
      stream: null,
      recording: false,
      recorder: null,
      audio: null,
      play: false,
      pause: true
    }
    this.url = 'http://streaming.tdiradio.com:8000/house.mp3'
    this.toggleRecorder.bind(this)
  }

  async componentDidMount() {
    let stream

    try {
      stream = await getAudioStream();
    } catch (error) {
      // Users browser doesn't support audio.
      // Add your handler here.
      console.log(error)
    }

    this.setState({ stream })
  }

  toggleRecorder() {
    const { recording } = this.state

    console.log('Toggle recording')
    if (recording)
      this.stopRecording()
    else
      this.startRecording()
  }

  startRecording() {
    console.log('start recording')
    const { stream } = this.state
    const audioContext = new (window.AudioContext || window.webkitAudioContext)()
    const recorder = new RecorderJS(audioContext)
    recorder.init(stream)

    this.setState(
      {
        recorder,
        recording: true
      },
      () => {
        recorder.start()
      }
    )
  }

  async stopRecording() {
    console.log('stop stopRecording')

    const { recorder } = this.state
    const { buffer } = await recorder.stop()
    console.log(buffer)
    const audio = exportBuffer(buffer[0])
    console.log(audio)
    const audioListen = new Audio(audio)
    const stream = new Audio(this.props.url)
    console.log('stream', stream)
    stream.play()
    console.log('audioListen', audioListen)
    this.setState({
      recording: false,
      audio: new Audio(audio)
    })
  }

  render() {
    const { stream, recording } = this.state
    if(!stream) return null

    return (
      <section className="hero">
        <div className="hero-body">
          <div className="container">
            <button
              onClick={() => { this.toggleRecorder() }}
              className={recording ? 'button has-text-grey-lighter' : 'button'}
              >{recording ? 'Stop Recording' : 'Start Recording'}</button>
            <div id="saved-audio-messages">
              <h2>Saved messages</h2>
            </div>
          </div>
        </div>
      </section>
    )
  }

}

class Maggie extends Component {
  render() {
    return (
      <section className="hero">
        <div className="hero-body">
          <div className="container">
            <h1 className="title has-text-centered">Maggie Client</h1>
            <section className="section">
              <div className="container has-text-centered">
                <MaggieRecorder />
              </div>
              <audio></audio>
            </section>
          </div>
        </div>
      </section>
    )
  }
}

export default Maggie